module FREST
  class BaseFunction
    def relation_fields
      {}
    end

    def fn &block
      @fn = block
    end

    def call storage:, **args
      new_args = args.reduce({}) do |h,(k, v)|
        h[k] = relation_fields[k]&.from_string(v, store: storage)
        h
      end

      @fn.(storage:, **new_args)
    end

    def thing_load(v, storage:)
      storage.reify v
    end

    class << self
      def register
        FREST::FunctionsDB.register(new)
      end
    end
  end
end