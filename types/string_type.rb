module FREST
  class StringType < Type
    class << self
      def from_string string, store: {}
        string
      end

      def id = 'string'

      def is_code = false
    end
  end
end

FREST::StringType.register