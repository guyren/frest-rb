require_relative('./type.rb')

module FREST
  class RelationType < Type

    class << self
      def from_string string, store: {}
        if string.is_a? String
          store[string]
        else
          string
        end
      end

      def id = 'relation'

      def is_code = false
    end
  end

end

FREST::RelationType.register