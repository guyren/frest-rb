require_relative('./type.rb')

module FREST
  class ModelType < Type

    class << self
      def from_string string, store: {}
        if string.is_a? String
          store[string]
        else
          string
        end
      end

      def id = 'model'

      def is_code = false
    end
  end

end

FREST::ModelType.register