require_relative './type.rb'

module FREST
  class IntType < Type
    class << self
      def from_string string, store: {}
        string.to_i
      end

      def id = 'int'

      def is_code = false
    end
  end
end

FREST::IntType.register
