module FREST
  class Type
    @@registry = {}

    def is_code
      raise 'Types must implement is_code'
    end

    def from_string string, store: {}
      raise 'Types must implement from_string'
    end

    class << self
      def register
        @@registry[id] = self
      end
    end
  end
end