require 'securerandom'

require './memory_db.rb'
require './functions_db.rb'

baseDB = FREST::MemoryDB.default
DB = baseDB + FREST::FunctionsDB.new(storage: baseDB)

RSpec::Matchers.define :ground_count_is do |count|
  match do |thing|
    DB.default['test_relation'].ground_count == count
  end
end


describe DB do
  context 'define a relation and use it' do
    before(:all) do
      @rel = DB.create_relation 'test_relation'
      @update_fields = DB[:update_fields]
      @define_field   = DB[:define_field]

      @define_field.(
        storage:  DB,
        relation: 'test_relation',
        name:     'a',
        type:     'int'
      )
      @define_field.(
        storage:  DB,
        relation: 'test_relation',
        name:     'b',
        type:     'string'
      )
      @update_fields.(
        storage:  DB,
        relation: @rel,
        model:    DB
      )
    end



    it 'can define a relation' do
      expect(@rel.field_count)
        .to equal(3)
    end

    it 'can insert into a relation' do
      expect(ground_count_is 0)

      uuid = SecureRandom.uuid

      row = @rel.new_row(id: uuid)

      row['a'] = 1
      expect(ground_count_is 0)

      row['b'] = 'test'
      expect(ground_count_is 1)
    end
  end
end