require './db_base.rb'

module FREST
  class FunctionsDB < DBBase
    attr_reader :functions

    def initialize storage: FunctionsDB.default
      FunctionsDB.default = storage
      @functions          = storage.create_relation('functions')
      @storage            = storage

      ['types/**/*', 'functions/**/*'].each do |pattern|
        Dir.glob(pattern).each do |f|
          require './' + f
        end
      end
    end

    def method_missing(symbol, *args, **kwargs)
      @functions[symbol.to_s]&.(**kwargs)
    end

    def [](id) = @functions[id]

    class << self
      def register fn, storage: default
        rel = storage.create_relation fn.id
        rel.define_fields fields: fn.relation_fields
        storage.functions[fn.id] = fn
      end

      def default
        @default ||= MemoryDB.new
      end

      def default=(default)
        @default = default
      end
    end
  end
end