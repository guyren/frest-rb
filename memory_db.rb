require './frest_relation.rb'
require './db_base.rb'

module FREST
  class MemoryDB < DBBase
    @databases = {}

    def initialize
      @relations = {
        # This gives me a syntax error on the & for some reason
        # 'thing' => MemoryRelation.new(notify: (&:new_thing))
        'thing' => MemoryRelation.new(notify: method(:new_thing))
      }

      @thing_relation = @relations['thing']
    end

    def [](relation_name)
      @relations[relation_name.to_s]
    end

    def create_relation name
      (result = (@relations[name.to_s] ||= FREST::MemoryRelation.new(name: name.to_s)))
        .define_fields(fields: {id: 'string'})

      result
    end

    def thing_relation(id) = @thing_relation[id.to_s]

    def thing(id)= thing_relation(id.to_s)[id.to_s]

    def method_missing(relation, *__)
      @relations[relation.to_s]
    end


    private

    def new_thing relation:, id:
      @thing_relation[id.to_s] = relation
    end
    
    class << self
      def default
        @default ||= new
        @databases['default'] = @default
        @default
      end

      def default=(default)
        @default = default
      end
    end
  end
end