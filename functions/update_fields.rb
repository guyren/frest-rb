require_relative '../base_function.rb'

module FREST
  class UpdateFieldsFn < BaseFunction
    def id = 'update_fields'

    def initialize
      fn do |relation:, model:, **args|
        #do nothing for in-memory
        p "*** Updating fields"
      end
    end

    def relation_fields = {
      relation: FREST::RelationType,
      model:    FREST::ModelType
    }
  end
end

FREST::UpdateFieldsFn.register