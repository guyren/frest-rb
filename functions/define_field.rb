require_relative '../base_function.rb'
require_relative '../types/relation_type.rb'
require_relative '../types/string_type.rb'

module FREST
  class DefineFieldFn < BaseFunction
    def id = 'define_field'

    def initialize
      fn do |relation:, name:, type:, **args|
        relation.define_field name:, type:
      end
    end

    def relation_fields = {
      relation: RelationType,
      name:     StringType,
      type:     StringType
    }
  end
end

FREST::DefineFieldFn.register