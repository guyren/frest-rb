module FREST
  class DBBase
    def +(preferredDB)
      JoinDB.new(self, preferredDB)
    end
  end

  class JoinDB < DBBase
    def initialize weaker, stronger
      @weaker   = weaker
      @stronger = stronger
    end

    def define_field relation:, name:, type:
      raise "This is a join of two databases. Add your fields to one of those."
    end

    def update_fields relation:
      raise "This is a join of two databases. Add your fields to one of those."
    end

    def field_count relation:
      @stronger.field_count(relation:) || @weaker.field_count(relation)
    end

    def ground_count relation:
      stronger = @stronger.ground_count(relation:)
      stronger == 0 ? @weaker.ground_count(relation:) : stronger
    end
    private def method_missing symbol, *args, **kwargs
      @stronger.send(symbol, *args, **kwargs) || @weaker.send(symbol, *args, **kwargs)
    end
  end
end