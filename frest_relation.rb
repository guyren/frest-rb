require './frest_namespace.rb'
require './db_base.rb'

module FREST
  class MemoryRelation < DBBase
    def initialize trigger: nil, name: nil, notify: ->(relation:, id:) {}
      @name     = name
      @trigger  = trigger
      @notify   = notify

      @fields   = {}
      @rows     = {}
    end

    def define_field name:, type:
      @fields[name] = type
    end

    def define_fields fields:
      fields.each {|name, type| define_field name:, type:}
    end

    def new_row(id:, **content)
      # TODO: make this an upsert
      result = FREST::Namespace.new id: id, fields: @fields, content: content
      @rows[id.to_s] = result
      @notify.(
        relation: self,
        id:
      )
      result
    end

    def [](id)
      @rows[id.to_s]
    end

    def []=(k, v)
      @rows[k] = v
    end

    def field_count
      @fields.count
    end

    def ground_count
      rows.count { |row| row.ground? }
    end
  end
end