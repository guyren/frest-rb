require './db_base.rb'

module FREST
  class TypesDB < DBBase
    attr_reader :types
    
    def initialize storage: TypesDB.default
      TypesDB.default = storage
      @types  = storage.create_relation('types')
      @storage    = storage

      Dir.glob('types/**/*').each do |f|
        require './' + f
      end
    end

    def method_missing(symbol, *args, **kwargs)
      @types[symbol.to_s]&.(**kwargs)
    end

    def [](id) = @types[id]


    class << self
      def register type, storage: default
        storage.types[type.id] = fn
      end

      def default
        @default ||= MemoryDB.new
      end

      def default=(default)
        @default = default
      end
    end
  end
end