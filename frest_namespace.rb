module FREST
  class Namespace
    def initialize(id:, fields: {}, content: {})
      @id = id
      @fields = fields
      @content = {}
    end

    def [](field)
      @content[field]
    end

    def []=(field, value)
      if @fields[field]
        # not sure if we should just ignore trying to set an already-set field
        @content[field] = value unless @content[field]
      else
        raise "No such field"
      end
    end

    def ground?
      fields.all {|name, _type| content[name] }
    end
  end
end